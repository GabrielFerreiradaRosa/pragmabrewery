import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import 'reset-css';

import './index.scss';

ReactDOM.render(<App />, document.getElementById('root'));
