import React from 'react';
import Notification from 'react-web-notification';

import BeerList from './components/BeerList';
import { BeerService } from './service/beer';

import './app.scss';

export default class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      beers: []
    };
  }

  async _getTemperatures() {
    const beers = await BeerService.get();

    this.setState({ beers });
  }

  _watchTemperatures() {
    setInterval(this._getTemperatures.bind(this), 60000);
  }

  _renderNotification() {
    const { beers } = this.state;
    const outOfRange = beers.filter(b => BeerService.calculateBeerOutOfRange(b));

    if (outOfRange.length > 0) {
      const options = {
        body: `The temperatures are outside the correct range: ${outOfRange.map(b => b.name).join(', ')}!`
      }

      return (
        <Notification
          options={options}
          timeout={10000}
          title="Brewery Notification"
        />
      );
    }

    return null;
  }

  componentDidMount() {
    this._getTemperatures();
    this._watchTemperatures();
  }

  render() {
    const { beers } = this.state;

    return (
      <div className="app">
        <BeerList beers={beers} />
        {this._renderNotification()}
      </div>
    );
  }
}
