const GET = 'GET';

export const Request = {

  get(url, params = {}, async = true) {
    return new Promise((resolve, reject) => {
      const xhr = new XMLHttpRequest();

      xhr.open(GET, this._formartUrl(url, params), async);

      xhr.onload = () => {
        const { status, readyState, response, responseURL, _url = '' } = xhr;

        if (status >= 200 && status < 300 && readyState === 4) {
          try {
            resolve({ status, responseURL: responseURL || _url, response: JSON.parse(response) });

          } catch (ex) {
            reject({ status: 404, response });
          }
        } else {
          reject({ status: 404, response });
        }
      }

      xhr.onerror = () => {
        const { status, statusText } = xhr;

        reject({ status, statusText });
      }

      xhr.send();
    });
  },

  _formartUrl(url, params) {
    return `${url}${this._formatParams(params)}`;
  },

  _formatParams(params) {
    const formatted = Object.keys(params).map(key => (key ? key + '=' + encodeURIComponent(params[key]) : '')).join('&');
    return formatted ? '?' + formatted : '';
  }
}
