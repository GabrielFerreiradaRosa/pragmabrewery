import React from 'react';
import PropTypes from 'prop-types';

import BeerCard from '../BeerCard';

import './main.scss';

export default function BeerList({ beers }) {

  function _renderBeers() {
    return beers.map(b => (
      <li className="beer-list__item" key={b.id}>
        <BeerCard name={b.name} min={b.min} max={b.max} current={b.current} />
      </li>
    ));
  }

  return (
    <ul className="beer-list">
      {_renderBeers()}
    </ul>
  );
}

BeerList.propTypes = {
  beers: PropTypes.array.isRequired
};
