import React from 'react';
import PropTypes from 'prop-types';

import './main.scss';

const MAX_DEGREE = 65;
const MIN_DEGREE = -65;

const mapBetween = (currentNum, minAllowed, maxAllowed, min, max) => {
  return (maxAllowed - minAllowed) * (currentNum- min) / (max - min) + minAllowed;
}

export default function Thermostat({ min, current, max }) {
  const indicatorPosition = mapBetween(current, MIN_DEGREE, MAX_DEGREE, min, max);

  return (
    <div className="thermostat">
      <div className="thermostat__core">
        <div className="thermostat__core__ring">
          <div className="thermostat__core__ring__triangle"></div>
        </div>
        <div className="thermostat__core__control">
          <div className="thermostat__core__control__indicator" style={{transform: `rotate(${indicatorPosition}deg)`}}></div>
          <div className="thermostat__core__control__up-temp">{max}º</div>
          <div className="thermostat__core__control__current-temp">{current}º</div>
          <div className="thermostat__core__control__down-temp">{min}º</div>
        </div>
      </div>
    </div>
  );
}

Thermostat.propTypes = {
  min: PropTypes.number.isRequired,
  max: PropTypes.number.isRequired,
  current: PropTypes.number.isRequired
};
