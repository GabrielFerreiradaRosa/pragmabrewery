import React from 'react';
import PropTypes from 'prop-types';

import Thermostat from '../Thermostat';
import { BeerService } from '../../service/beer';

import './main.scss';

export default function BeerCard({ name, min, max, current }) {
  const isOutOfRange = BeerService.calculateBeerOutOfRange({ min, max, current });

  return (
    <div className={`beer-card ${isOutOfRange ? 'red' : ''}`}>
      <h3 className="beer-card__title">{name}</h3>
      <Thermostat min={min} current={current} max={max} />
    </div>
  );
}

BeerCard.propTypes = {
  name: PropTypes.string.isRequired,
  min: PropTypes.number.isRequired,
  max: PropTypes.number.isRequired,
  current: PropTypes.number.isRequired
};
