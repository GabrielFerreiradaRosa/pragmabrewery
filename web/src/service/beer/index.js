import { Request } from '../../util/request';

export const BeerService = {
  async get() {
    const { response } = await Request.get('/beer');

    return response;
  },

  calculateBeerOutOfRange(beer) {
    return beer.current > beer.max || beer.current < beer.min;
  }
}
