import BeerList from '../../src/components/BeerList';

describe('<BeerList />', () => {
  const props = {
    beers: [{ id: 0, name: 'Ipa', min: 1.0, max: 10, current: 5 }]
  };

  test('check props', () => {
    const component = mount(<BeerList {...props} />);

    expect(component.props().beers).toBe(props.beers);
  });

});
