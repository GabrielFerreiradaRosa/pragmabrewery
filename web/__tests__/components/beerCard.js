import BeerCard from '../../src/components/BeerCard';

describe('<BeerCard />', () => {

  test('check props', () => {
    const props = { name: 'Ipa', min: 1.0, max: 10, current: 5 };
    const component = mount(<BeerCard {...props} />);

    expect(component.props().name).toBe(props.name);
    expect(component.props().min).toBe(props.min);
    expect(component.props().max).toBe(props.max);
    expect(component.props().current).toBe(props.current);
  });

  test('check title by class', () => {
    const props = { name: 'Ipa', min: 1.0, max: 10, current: 5 };
    const component = mount(<BeerCard {...props} />);

    expect(component.render().find('.beer-card__title').length).toBe(1);
  });

  test('check Thermostat component by class', () => {
    const props = { name: 'Ipa', min: 1.0, max: 10, current: 5 };
    const component = mount(<BeerCard {...props} />);

    expect(component.render().find('.thermostat').length).toBe(1);
  });

  test('check class red when the are outside the correct range', () => {
    const props = { name: 'Ipa', min: 1.0, max: 10, current: 11 };
    const component = mount(<BeerCard {...props} />);

    expect(component.render().hasClass('red')).toBe(true);
  });

  test('does not have class red when the are inside the correct range', () => {
    const props = { name: 'Ipa', min: 1.0, max: 10, current: 7 };
    const component = mount(<BeerCard {...props} />);

    expect(component.render().hasClass('red')).toBe(false);
  });

});
