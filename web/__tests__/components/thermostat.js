import Thermostat from '../../src/components/Thermostat';

describe('<Thermostat />', () => {
  const props = { min: 1.0, max: 10, current: 5 };

  test('check props', () => {
    const component = mount(<Thermostat {...props} />);

    expect(component.props().min).toBe(props.min);
    expect(component.props().max).toBe(props.max);
    expect(component.props().current).toBe(props.current);
  });

  test('check control by class', () => {
    const component = mount(<Thermostat {...props} />);

    expect(component.render().find('.thermostat__core__control').length).toBe(1);
  });

});
