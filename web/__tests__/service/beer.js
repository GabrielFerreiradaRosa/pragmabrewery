import { BeerService } from '../../src/service/beer';
import { Request } from '../../src/util/request';

describe('Beer Service', () => {
  test('method: get must call Request.get once', () => {
    const mock = jest.spyOn(Request, 'get');

    BeerService.get();

    expect(mock).toHaveBeenCalledTimes(1);
  });

  test('method: get must return correct data', async () => {
    const data = { response: 'test' };

    jest.spyOn(Request, 'get').mockReturnValue(Promise.resolve(data));

    const ret = await BeerService.get();

    expect(ret).toBe(data.response);
  });

  test('method: calculateBeerOutOfRange must return false when receive min: 1, max: 10, current: 5', async () => {
    const data = { min: 1, max: 10, current: 5 };
    const ret = BeerService.calculateBeerOutOfRange(data);

    expect(ret).toBe(false);
  });

  test('method: calculateBeerOutOfRange must return true when receive min: 1, max: 10, current: 15', async () => {
    const data = { min: 1, max: 10, current: 15 };
    const ret = BeerService.calculateBeerOutOfRange(data);

    expect(ret).toBe(true);
  });

  test('method: calculateBeerOutOfRange must return true when receive min: 1, max: 10, current: -2', async () => {
    const data = { min: 1, max: 10, current: 15 };
    const ret = BeerService.calculateBeerOutOfRange(data);

    expect(ret).toBe(true);
  });

});
