import App from '../src/App';

describe('<App />', () => {
  const mock = {
    beers: [{ id: 0, name: 'Ipa', min: 1.0, max: 10, current: 5 }]
  };

  test('methods: componentDidMount calls _getTemperatures when called', () => {
    jest.spyOn(App.prototype, '_getTemperatures');
    mount(<App />);

    expect(App.prototype._getTemperatures).toHaveBeenCalled();
  });

  test('methods: componentDidMount calls _watchTemperatures when called', () => {
    jest.spyOn(App.prototype, '_watchTemperatures');
    mount(<App />);

    expect(App.prototype._watchTemperatures).toHaveBeenCalled();
  });

  test('check render <BeerList />', () => {
    const component = mount(<App />);

    expect(component.find('.beer-list__item').length).toBe(0);

    component.setState({ beers: mock.beers });

    expect(component.find('.beer-list__item').length).toBe(1);
  });

});
