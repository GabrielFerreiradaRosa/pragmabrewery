# Pragma Brewery

## About
Solution that allows to be aware of the current temperature of each container and notifies when the temperatures are outside the correct range.

## Technologies

* React
* Webpack
* Jest
* Sass
* NodeJs
* Express
* Babel

## Requirements

* Node v10.15.3 or higher
* Yarn 1.17.3 or higher

## Run

```
./build.sh
```

## Questions

**What are the highlights of your logic/code writing style?**

Simple code, easy to maintain and small components.

**What could have been done in a better way? What would you do in version 2.0?**

For future versions, I would like make a website responsive, with PWA and websocket communication.

**What were the questions you would ask and your own answers/assumptions?**

Should it be better design?

I think, it is not necessary for now, because it is a first version.

Should there be a section to add more beers?

In the second version would be a requirement.

**Tip**
There is an update every 10000ms.
