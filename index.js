const express = require('express')
const bodyParser = require('body-parser')
const staticRouter = require('./src/static')
const router = require('./src/router')

const app = express()

app.use(bodyParser.json())

staticRouter(app)
router(app)

app.listen(3000, () => console.info('API Listening on port 3000!'))

module.exports = app