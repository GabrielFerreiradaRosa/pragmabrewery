function mockCurrentTemperature(beer) {
  let { min, max } = beer

  min -=1
  max +=1

  return Math.floor(Math.random() * (max - min + 1) + min)
}

const BeerService = {
  getBeers() {
    const data = [
      { id: 0, name: 'Pilsner', min: -6.0, max: 4.0 },
      { id: 1, name: 'IPA', min: -6.0, max: 5.0 },
      { id: 2, name: 'Lager', min: -7.0, max: 4.0 },
      { id: 3, name: 'Stout', min: -8.0, max: 6.0 },
      { id: 4, name: 'Wheat beer', min: -5.0, max: 3.0 },
      { id: 5, name: 'Pale Ale', min: -6.0, max: 4.0 }
    ]

    data.forEach(b => b.current = mockCurrentTemperature(b))

    return data
  }
}

module.exports = BeerService
