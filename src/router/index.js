const Beer = require('./beer')

function router(app) {
  Beer.routers(app)
}

module.exports = router
