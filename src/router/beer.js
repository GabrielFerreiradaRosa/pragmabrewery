const BeerService = require('../service/beer')

const Beer = {
  _get(req, res) {
    try {
      res.json(BeerService.getBeers())
    } catch (e) {
      res.status(500).send(e)
    }
  },

  routers(app) {
    app.get('/beer', this._get)

    return app
  }
}

module.exports = Beer
