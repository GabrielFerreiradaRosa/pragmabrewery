const express = require('express')
const path = require('path')

function staticRouter(app) {
	app.use('/', express.static(path.join(__dirname, '../../web/dist')))
}

module.exports = staticRouter
